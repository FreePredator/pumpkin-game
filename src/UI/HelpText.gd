extends Node2D

onready var label = get_child(0).get_child(0)


func show_text(text):
	label.text = text
	label.show()

func hide_text():
	label.hide()
