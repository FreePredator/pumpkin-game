extends Node2D

var player_near
var on = false
onready var gm = get_node("/root/GameManager")


func _on_Area2D_body_entered(body):
	if body == get_node("/root/Level" + str(gm.level) + "/KinematicBody2D"):
		get_node("/root/Level" + str(gm.level) + "/Text").show_text("Press q to light the lantern!")
		player_near = true

func _on_Area2D_body_exited(body):
	if body == get_node("/root/Level" + str(gm.level) + "/KinematicBody2D"):
		get_node("/root/Level" + str(gm.level) + "/Text").hide_text()
		player_near = false

func _physics_process(_delta):
	if Input.is_action_just_pressed("light") && player_near:
		get_node("/root/Audio/Lantern").play(0.05)
		light()
		
func light():
	if !on:
		self.get_child(1).show()
		self.get_child(0).modulate = Color("feff67")
		on = true
	elif on:
		self.get_child(1).hide()
		self.get_child(0).modulate = Color("060606")
		on = false
