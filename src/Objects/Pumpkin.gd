extends Node2D

onready var gm = get_node("/root/GameManager")
var player_near = false

func _on_Area2D_body_entered(_body):
	player_near = true
	get_node("/root/Level" + str(gm.level) + "/Text").show_text("Press q to switch to the next level")

func _on_Area2D_body_exited(_body):
	player_near = false

func _physics_process(_delta):
	if Input.is_action_just_pressed("light") && player_near:
		gm.next_level()
