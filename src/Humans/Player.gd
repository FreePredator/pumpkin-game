extends KinematicBody2D

var grv = 2000
var velo = Vector2.ZERO
var speed = 15000
var height = 33000
var jumping
var timer

func _ready():
	timer = Timer.new()
	add_child(timer)

func _physics_process(delta):
	var input = (Input.get_action_strength("right") - Input.get_action_strength("left")) * speed
	
	if input < 1 && input != 0:
		get_child(3).hide()
		get_child(2).show()
		if is_on_floor():
			get_child(2).play()
			get_child(3).stop()
	elif input > 1 && input != 0:
		get_child(2).hide()
		get_child(3).show()
		if is_on_floor():
			get_child(3).play()
			get_child(2).stop()
	elif input == 0:
		get_child(2).stop()
		get_child(2).frame = 0
		get_child(3).stop()
		get_child(3).frame = 0

	var jump = 0
	if Input.is_action_just_pressed("up") && !jumping:
		jump = -height

	if jump == 0:
		velo.y += grv * delta
	elif is_on_floor(): 
		velo.y = jump * delta

	if is_on_floor() &&	!get_node("/root/Audio/Walk").playing && input != 0 && timer.get_time_left() <= 0.09:
		timer.set_wait_time(.5)
		timer.stop()
		timer.start()
		get_node("/root/Audio/Walk").play()

	velo.x = input * delta

	velo = move_and_slide(velo, Vector2.UP)


