extends Node2D

export var to_right: int
var other_pos
var pos
export var speed: int
var dir = 1
var velo
onready var gm = get_node("/root/GameManager")
var timer
var killed_player

func _ready():
	timer = Timer.new()
	add_child(timer)
	timer.set_wait_time(1)
	timer.start()
	pos = self.position.x
	other_pos = self.position.x + to_right
	if other_pos < pos:
		var temp = pos
		pos = other_pos
		other_pos = temp


func _physics_process(_delta):
	if self.position.x >= other_pos:
		dir = -1
	if self.position.x <= pos:
		dir = 1

	if dir == 1:
		self.position.x += speed
	elif dir == -1:
		self.position.x -= speed


func _on_Area2D_body_entered(body):
	var player = get_node("/root/Level" + str(gm.level) + "/KinematicBody2D") 
	if body == player:
		get_node("/root/Audio/Death").play()
		get_tree().reload_current_scene()
