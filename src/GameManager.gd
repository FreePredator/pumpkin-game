extends Node2D

var level = 1
var timer
var win = false

func _ready():
	timer = Timer.new()
	add_child(timer)

func next_level():
	if level == 3:
		get_node("/root/Level" + str(level) + "/Text").show_text("You win!")
		win = true
		timer.set_wait_time(2)
		timer.start()
	else:
		level = level + 1
		get_tree().change_scene("res://src/Levels/Level" + str(level) + ".tscn")
		get_node("/root/Audio/End").play()

func _process(_delta):
	if win && timer.get_time_left() <= 0.1:
		get_tree().quit()
