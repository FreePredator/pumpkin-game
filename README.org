#+TITLE: Find the pumpkin


This is a game I made in 2 days for the [[https://itch.io/jam/spooky-2d-jam-2022][Spooky 2D Jam '22]].
The theme of this game is *What's hiding in the dark?*

You can download binaries or play the game in the browser here:
[[https://fpgames183.itch.io/find-the-pumpkin][https://fpgames183.itch.io/find-the-pumpkin]]

[[./img/Screenshot.png]]
